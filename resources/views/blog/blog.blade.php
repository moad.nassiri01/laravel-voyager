@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-8">        
            <h1>NEW BLOG</h1>

            @foreach ($mypost as $post)    

                <div class="card text-white bg-primary mt-4">
                   <a href="/post/{{ $post->slug }}"> <img class="card-img-top" src="{{ asset('/storage/'.$post->image)}}" alt=""> </a>
                    <div class="card-body">
                    <h4 class="card-title">{{ $post->title}}</h4>
                    <p class="card-text">{{ Str::limit($post->excerpt,50) }}</p>
                    <p class="card-text"> <a href="{{ url('/blog/'.$post->category->id.'') }}"> {{ $post->category->name }} </a> </p>
                    </div>
                </div>

            @endforeach

                <div class="row container my-4">
                
                    <div class="pagination">

                        {{ $mypost->links() }}

                    </div>

                </div>

                
        </div>

            <div class="col-lg-4">

                <div class="list-group">
                        
                        <a href="{{ url ('/blog') }}" class="list-group-item d-flex justify-content-between align-items-center @if (!$id ) active @endif">
                            
                            All
                            <span class="badge badge-primary badge-pill">{{  $count }}</span>
                        </a>

                        @foreach ($category as $categorie)
                        
                            <a href="{{ url ('/blog/'.$categorie->id.'') }}" class="list-group-item d-flex justify-content-between align-items-center @if ($categorie->id == $id ) active @endif">
                            
                                {{$categorie->name}}    
                                <span class="badge badge-primary badge-pill">{{ $categorie->posts->count() }}</span>
                            
                            </a>


                    
                        @endforeach
                </div>

            </div>
        
    </div>

@endsection