@extends('master')


@section('css')

 
@endsection



@section('slider')
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">

            @foreach ($mypost as $post)

            <div class="carousel-item @if ( $loop->first) active @endif">
                
                <img class="d-block w-100"  src="{{ asset('/storage/'.$post->image)}}" alt="">
            
            </div>

            @endforeach

         </div>
        
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>

    @endsection

@section('content')



    <div class="row">
        <div class="col-md-12">        

            <h1>Listes Articles</h1>
            
            <div class="row ">
                
                @foreach ($mypost as $post)

            
                <div class="col-lg-4 mt-4">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('/storage/'.$post->image)}}" alt="">
                        <div class="card-body">
                            <h4 class="card-title">{{ $post->title }}</h4>
                            <p class="card-text">{{ Str::limit($post->excerpt,50) }}</p>
                        </div>
                    </div>
                </div>
           
            
            @endforeach
            </div>
            
        </div>
    </div>
@endsection

@section('javascript')



@endsection