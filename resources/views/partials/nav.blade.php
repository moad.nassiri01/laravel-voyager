
<?php  $currentPath= URL::current(); 

    $url_active=explode("/",$currentPath);

    $url_active="/".end($url_active);
?>

<ul class="navbar-nav mr-auto">


@foreach ($items as $item)

      <li class="nav-item  @if ($url_active==$item->url) active @endif">
        <a class="nav-link" href="{{ $item->url }}">{{ $item->title }} <span class="sr-only">(current)</span></a>
      </li>

@endforeach
    </ul>