<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->take(6)->get();

        return view('blog.index', ['mypost'=>$posts]);
    }


    public function blog($id=null)
    {       
            if ($id){

                $posts = Post::orderBy('created_at','desc')->where('status','published')->where('category_id',$id)->paginate(2);


            }else {

                $posts = Post::orderBy('created_at','desc')->where('status','published')->paginate(2);

            }
            
            $count_post=Post::all()->count();
            $category=Category::all();
            return view('blog.blog', ['id'=>$id, 'mypost'=>$posts, 'category'=>$category, 'count'=>$count_post]);
    }

    public function show($slug, $id=null){

        if ($id){

            $posts = Post::orderBy('created_at','desc')->where('status','published')->where('category_id',$id)->paginate(2);


        }else {

            $posts = Post::orderBy('created_at','desc')->where('status','published')->paginate(2);

        }

        $count_post=Post::all()->count();
            $category=Category::all();

        $post = Post::whereSlug($slug)->first();
        return view('blog.show', ['post' => $post, 'id'=>$id, 'mypost'=>$posts, 'category'=>$category, 'count'=>$count_post ] );



    }
    
}
